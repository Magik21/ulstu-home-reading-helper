# ULSTU Home Reading Helper
Requirements:

    - Python 3


## Guide
### Backend
__Pipenv is required! How to install pipenv?__  

For all users, requires admin rights
```shell script
pip install pipenv
```

For current user  
Make sure python scripts folder is in PATH  
else you might be unable to write `pipenv` without `python -m`  
```shell script
pip install --user pipenv
```  
#### Installing requirements
```shell script  
pipenv install  
```  
#### Running  
```shell script  
pipenv run serve  
```  
