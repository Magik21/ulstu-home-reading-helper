const PLEASE_WAIT_MESSAGE = '...';
const SEND_DELAY = 500;


let checkTimeoutId;
input.oninput = () => {
    clearTimeout(checkTimeoutId);
    if (!input.value) {
        words.innerHTML = total.innerHTML = '';
        return;
    }

    const data = new FormData();
    data.set('text', input.value);

    words.innerHTML = total.innerHTML = PLEASE_WAIT_MESSAGE;
    checkTimeoutId = setTimeout(async () => {
        const response = await fetch('/api/check', {
            'method': 'POST',
            'body': data,
        });
        const result = await response.json();
        words.innerHTML = result.words;
        total.innerHTML = result.total;
    }, SEND_DELAY);
};


outputHideBtn.onclick = () => {
    getSelection().removeAllRanges();
    outputForm.hidden = true;
};


let translateTimeoutId;
input.onselect = () => {
    clearTimeout(translateTimeoutId);
    const data = new FormData();
    data.set('text', getSelection().toString());

    translateTimeoutId = setTimeout(async () => {
        const response = await fetch('/api/translate', {
            method: 'POST',
            body: data,
        });

        switch (response.status) {
            case 400:
                console.log('Неправильный запрос. Скорее всего.');
                return;
            case 404:
                console.log('Перевод не найден!');
                return;
            case 500:
                console.log('Ошибка Yandex API');
                return;
        }

        const result = await response.json();
        if (getSelection().toString()) {
            // Show only if text still selected
            if (result.transcription) {
                output.value = `${result.text} [${result.transcription}] — ${result.translation}`;
            } else {
                output.value = result.translation;
            }
            outputForm.hidden = false;
        }
    }, SEND_DELAY);
};
