from string import whitespace

from flask import Blueprint, request, abort, jsonify
from requests import post
from requests.exceptions import ConnectionError

import config


api = Blueprint('api', __name__)


@api.after_request
def set_crossdomain_headers(response):
    """This middleware provides cross-domain requests"""
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response


@api.route('/check', methods=['POST'])
def api_check():
    """Computes text stats
    :Errors:
        400: Empty text
    :returns: JSON {
        "total": int,
        "words": int
    }
    """

    try:
        text = request.form['text']
    except KeyError:
        abort(400)

    total = 0
    for symbol in text:
        if symbol.isalpha() or symbol.isdigit():
            total += 1
    
    return jsonify({
        'total': total,
        'words': len(text.split()),
    })


@api.route('/translate', methods=['POST'])
def api_translate():
    """Translates the text
    Uses Yandex.Dict to translate a single word
    Uses Yandex.Translate to translate the sentence

    :Errors:
        400: Empty text
        404: Definition not found
        500: Failed to connect to Yandex
    :returns: JSON {
        "text": str,
        "transcription": str or null,
        "translation": str
    }
    """

    try:
        text = request.form['text'].strip()
    except KeyError:
        abort(400)
    if not text:
        abort(400)
    
    request_data = {
        'text': text,
        'lang': 'en-ru',
    }

    # If any whitespace symbol is in text then count of words > 1
    if any(symbol in text for symbol in whitespace):
        request_data['key'] = config.YANDEX_TRANSLATE_KEY

        try:
            with post(config.YANDEX_TRANSLATE_API_URL, data=request_data) as res:
                parsed_response = res.json()
        except ConnectionError:
            abort(500)

        return jsonify({
            'text': text,
            'translation': parsed_response['text'][0],
            'transcription': None,
        })
    else:
        request_data['key'] = config.YANDEX_DICT_KEY

        try:
            with post(config.YANDEX_DICT_API_URL, data=request_data) as res:
                parsed_response = res.json()
        except ConnectionError:
            abort(500)

        if not parsed_response['def']:
            abort(404)
        
        definition = parsed_response['def'][0]
        word = definition['text']
        transcription = definition.get('ts')
        translation = definition['tr'][0]['text']

        return jsonify({
            'text': word,
            'transcription': transcription,
            'translation': translation,
        })
