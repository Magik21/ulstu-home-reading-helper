from flask import Flask

from config import STATIC_FOLDER
from api import api
from views import views


app = Flask(__name__, static_folder=STATIC_FOLDER)
app.register_blueprint(api, url_prefix='/api/')
app.register_blueprint(views)
