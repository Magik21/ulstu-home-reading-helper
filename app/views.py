from flask import Blueprint, send_from_directory
from config import STATIC_FOLDER


views = Blueprint('views', __name__)


@views.route('/')
def route_index():
    return send_from_directory(STATIC_FOLDER, 'index.html')
