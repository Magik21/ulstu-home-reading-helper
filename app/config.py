from os.path import join
from os import getcwd


STATIC_FOLDER = join(getcwd(), 'static')
YANDEX_DICT_KEY = 'dict.1.1.20200416T222934Z.c8522a39cf394d83.946902eb163b36a08a7b22c0b1ee6b4334d1b45d'
YANDEX_DICT_API_URL = 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup'
YANDEX_TRANSLATE_KEY = 'trnsl.1.1.20200416T214249Z.79c45503f794a4bc.817dd819c1770b6f73be0301a6e014d8893a3270'
YANDEX_TRANSLATE_API_URL = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
